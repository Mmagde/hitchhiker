<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCitytripTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
    		if (!Schema::hasColumn("trip", "city_id"))
		{
	Schema::table("trip", function (Blueprint $table)  {
		$table->integer("city_id")->unsigned();
		$table->foreign("city_id")->references("id")->on("city")->onDelete("cascade");

	});		}

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
    	Schema::disableForeignKeyConstraints();
		if (Schema::hasColumn("trip", "city_id"))
		{
			$arrayOfKeys = $this->listTableForeignKeys("trip");
			Schema::table("trip", function ($table) use ($arrayOfKeys) {
			Schema::disableForeignKeyConstraints();
				if(in_array("trip_city_id_foreign" , $arrayOfKeys)){
					$table->dropForeign("trip_city_id_foreign");
					$table->dropColumn("city_id");
				}else{
					$table->dropColumn("city_id");
				}
			Schema::enableForeignKeyConstraints();
			});
		}
		Schema::enableForeignKeyConstraints();

    }
}
