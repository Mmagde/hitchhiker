<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRequeststatusrequestTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
    		if (!Schema::hasColumn("request", "requeststatus_id"))
		{
	Schema::table("request", function (Blueprint $table)  {
		$table->integer("requeststatus_id")->unsigned();
		$table->foreign("requeststatus_id")->references("id")->on("requeststatus")->onDelete("cascade");

	});		}

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
    	Schema::disableForeignKeyConstraints();
		if (Schema::hasColumn("request", "requeststatus_id"))
		{
			$arrayOfKeys = $this->listTableForeignKeys("request");
			Schema::table("request", function ($table) use ($arrayOfKeys) {
			Schema::disableForeignKeyConstraints();
				if(in_array("request_requeststatus_id_foreign" , $arrayOfKeys)){
					$table->dropForeign("request_requeststatus_id_foreign");
					$table->dropColumn("requeststatus_id");
				}else{
					$table->dropColumn("requeststatus_id");
				}
			Schema::enableForeignKeyConstraints();
			});
		}
		Schema::enableForeignKeyConstraints();

    }
}
