<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateContractstatuscontractTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
    		if (!Schema::hasColumn("contract", "contractstatus_id"))
		{
	Schema::table("contract", function (Blueprint $table)  {
		$table->integer("contractstatus_id")->unsigned();
		$table->foreign("contractstatus_id")->references("id")->on("contractstatus")->onDelete("cascade");

	});		}

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
    	Schema::disableForeignKeyConstraints();
		if (Schema::hasColumn("contract", "contractstatus_id"))
		{
			$arrayOfKeys = $this->listTableForeignKeys("contract");
			Schema::table("contract", function ($table) use ($arrayOfKeys) {
			Schema::disableForeignKeyConstraints();
				if(in_array("contract_contractstatus_id_foreign" , $arrayOfKeys)){
					$table->dropForeign("contract_contractstatus_id_foreign");
					$table->dropColumn("contractstatus_id");
				}else{
					$table->dropColumn("contractstatus_id");
				}
			Schema::enableForeignKeyConstraints();
			});
		}
		Schema::enableForeignKeyConstraints();

    }
}
