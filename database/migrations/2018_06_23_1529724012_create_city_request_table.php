<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCityrequestTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
    		if (!Schema::hasColumn("request", "city_id"))
		{
	Schema::table("request", function (Blueprint $table)  {
		$table->integer("city_id")->unsigned();
		$table->foreign("city_id")->references("id")->on("city")->onDelete("cascade");

	});		}

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
    	Schema::disableForeignKeyConstraints();
		if (Schema::hasColumn("request", "city_id"))
		{
			$arrayOfKeys = $this->listTableForeignKeys("request");
			Schema::table("request", function ($table) use ($arrayOfKeys) {
			Schema::disableForeignKeyConstraints();
				if(in_array("request_city_id_foreign" , $arrayOfKeys)){
					$table->dropForeign("request_city_id_foreign");
					$table->dropColumn("city_id");
				}else{
					$table->dropColumn("city_id");
				}
			Schema::enableForeignKeyConstraints();
			});
		}
		Schema::enableForeignKeyConstraints();

    }
}
