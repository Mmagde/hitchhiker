<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsertripTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
    		if (!Schema::hasColumn("trip", "user_id"))
		{
	Schema::table("trip", function (Blueprint $table)  {
		$table->integer("user_id")->unsigned();
		$table->foreign("user_id")->references("id")->on("users")->onDelete("cascade");

	});		}

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
    	Schema::disableForeignKeyConstraints();
		if (Schema::hasColumn("trip", "user_id"))
		{
			$arrayOfKeys = $this->listTableForeignKeys("trip");
			Schema::table("trip", function ($table) use ($arrayOfKeys) {
			Schema::disableForeignKeyConstraints();
				if(in_array("trip_user_id_foreign" , $arrayOfKeys)){
					$table->dropForeign("trip_user_id_foreign");
					$table->dropColumn("user_id");
				}else{
					$table->dropColumn("user_id");
				}
			Schema::enableForeignKeyConstraints();
			});
		}
		Schema::enableForeignKeyConstraints();

    }
}
