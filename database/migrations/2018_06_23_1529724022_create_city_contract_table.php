<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCitycontractTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
    		if (!Schema::hasColumn("contract", "city_id"))
		{
	Schema::table("contract", function (Blueprint $table)  {
		$table->integer("city_id")->unsigned();
		$table->foreign("city_id")->references("id")->on("city")->onDelete("cascade");

	});		}

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
    	Schema::disableForeignKeyConstraints();
		if (Schema::hasColumn("contract", "city_id"))
		{
			$arrayOfKeys = $this->listTableForeignKeys("contract");
			Schema::table("contract", function ($table) use ($arrayOfKeys) {
			Schema::disableForeignKeyConstraints();
				if(in_array("contract_city_id_foreign" , $arrayOfKeys)){
					$table->dropForeign("contract_city_id_foreign");
					$table->dropColumn("city_id");
				}else{
					$table->dropColumn("city_id");
				}
			Schema::enableForeignKeyConstraints();
			});
		}
		Schema::enableForeignKeyConstraints();

    }
}
