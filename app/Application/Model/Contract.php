<?php
 namespace App\Application\Model;
 use Illuminate\Database\Eloquent\Model;
 class Contract extends Model
{
   public $table = "contract";
   public function user(){
		return $this->belongsTo(User::class, "user_id");
		}
  public function from()
  {
    return $this->belongsTo(City::class, "from_id");
  }
  public function to()
  {
    return $this->belongsTo(City::class, "to_id");
  }
  public function hicker()
  {
    return $this->belongsTo(User::class, "hicker_id");
  }
   public function contractstatus(){
  return $this->belongsTo(Contractstatus::class, "contractstatus_id");
  }
     protected $fillable = [
     'user_id',
    'from_id',
    'to_id',
    'hicker_id',
    'contractstatus_id',
    'price','status'
   ];
  }
