<?php
 namespace App\Application\Model;
 use Illuminate\Database\Eloquent\Model;
 class Contractstatus extends Model
{
   public $table = "contractstatus";
  public function contract(){
		return $this->hasMany(Contract::class, "contractstatus_id");
		}
     protected $fillable = [
        'name'
   ];
  }
