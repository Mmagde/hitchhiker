<?php
 namespace App\Application\Model;
 use Illuminate\Database\Eloquent\Model;
 class Requeststatus extends Model
{
   public $table = "requeststatus";
  public function request(){
		return $this->hasMany(Request::class, "requeststatus_id");
		}
     protected $fillable = [
        'name'
   ];
  }
