<?php
 namespace App\Application\Model;
 use Illuminate\Database\Eloquent\Model;
 class Trip extends Model
{
   public $table = "trip";
   public function from(){
		return $this->belongsTo(City::class, "from_id");
    }
    public function to()
    {
      return $this->belongsTo(City::class, "to_id");
    }
   public function user(){
  return $this->belongsTo(User::class, "user_id");
  }
     protected $fillable = [
        'from_id',
        'to_id',
        'user_id',
        'phone','date','price'
   ];
  }
