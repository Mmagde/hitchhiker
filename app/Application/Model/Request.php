<?php
 namespace App\Application\Model;
 use Illuminate\Database\Eloquent\Model;
 class Request extends Model
{
   public $table = "request";
  public function from()
  {
    return $this->belongsTo(City::class, "from_id");
  }
  public function to()
  {
    return $this->belongsTo(City::class, "to_id");
  }
   public function user(){
  return $this->belongsTo(User::class, "user_id");
  }
  public function hicker()
  {
    return $this->belongsTo(User::class, "hicker_id");
  }
   public function requeststatus(){
  return $this->belongsTo(Requeststatus::class, "requeststatus_id");
  }
     protected $fillable = [
    'from_id',
    'to_id',
     'user_id',
    'hicker_id',
   'requeststatus_id',
        'price','status'
   ];
  }
