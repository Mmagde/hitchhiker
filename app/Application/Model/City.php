<?php
 namespace App\Application\Model;
 use Illuminate\Database\Eloquent\Model;
 class City extends Model
{
   public $table = "city";
   public function contract(){
		return $this->hasMany(Contract::class, "city_id");
		}
   public function request(){
  return $this->hasMany(Request::class, "city_id");
  }
  public function trip(){
  return $this->hasMany(Trip::class, "city_id");
  }
     protected $fillable = [
        'name'
   ];
  }
