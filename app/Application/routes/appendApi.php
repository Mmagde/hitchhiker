<?php

#user
Route::post('users/login', 'UserApi@login');
Route::get('users/getById/{id}', 'UserApi@getById');
Route::get('users/delete/{id}', 'UserApi@delete');
Route::post('users/add', 'UserApi@add');
Route::post('users/update', 'UserApi@update');
Route::get('users', 'UserApi@index');
Route::get('users/getUserByToken', 'UserApi@getUserByToken');

#page
Route::get('page/getById/{id}', 'PageApi@getById');
Route::get('page/delete/{id}', 'PageApi@delete');
Route::post('page/add', 'PageApi@add');
Route::post('page/update/{id}', 'PageApi@update');
Route::get('page', 'PageApi@index');

#categorie
Route::get('categorie/getById/{id}', 'CategorieApi@getById');
Route::get('categorie/delete/{id}', 'CategorieApi@delete');
Route::post('categorie/add', 'CategorieApi@add');
Route::post('categorie/update/{id}', 'CategorieApi@update');
Route::get('categorie', 'CategorieApi@index');



#city
Route::get('city/getById/{id}', 'CityApi@getById');
Route::get('city/delete/{id}', 'CityApi@delete');
Route::post('city/add', 'CityApi@add');
Route::post('city/update/{id}', 'CityApi@update');
Route::get('city', 'CityApi@index');

#type
Route::get('type/getById/{id}', 'TypeApi@getById');
Route::get('type/delete/{id}', 'TypeApi@delete');
Route::post('type/add', 'TypeApi@add');
Route::post('type/update/{id}', 'TypeApi@update');
Route::get('type', 'TypeApi@index');

#requeststatus
Route::get('requeststatus/getById/{id}', 'RequeststatusApi@getById');
Route::get('requeststatus/delete/{id}', 'RequeststatusApi@delete');
Route::post('requeststatus/add', 'RequeststatusApi@add');
Route::post('requeststatus/update/{id}', 'RequeststatusApi@update');
Route::get('requeststatus', 'RequeststatusApi@index');

#contractstatus
Route::get('contractstatus/getById/{id}', 'ContractstatusApi@getById');
Route::get('contractstatus/delete/{id}', 'ContractstatusApi@delete');
Route::post('contractstatus/add', 'ContractstatusApi@add');
Route::post('contractstatus/update/{id}', 'ContractstatusApi@update');
Route::get('contractstatus', 'ContractstatusApi@index');

#trip
Route::get('trip/getById/{id}', 'TripApi@getById');
Route::get('trip/delete/{id}', 'TripApi@delete');
Route::post('trip/add', 'TripApi@add');
Route::post('trip/search', 'TripApi@search');
Route::post('trip/update/{id}', 'TripApi@update');
Route::get('trip', 'TripApi@index');

#request
Route::get('request/getById/{id}', 'RequestApi@getById');
Route::get('request/delete/{id}', 'RequestApi@delete');
Route::post('request/add', 'RequestApi@add');
Route::get('request/pending', 'RequestApi@pending');
Route::post('request/accept', 'RequestApi@accept');
Route::post('request/reject', 'RequestApi@reject');
Route::post('request/update/{id}', 'RequestApi@update');
Route::get('request', 'RequestApi@index');

#contract
Route::get('contract/getById/{id}', 'ContractApi@getById');
Route::get('contract/delete/{id}', 'ContractApi@delete');
Route::post('contract/add', 'ContractApi@add');
Route::post('contract/update/{id}', 'ContractApi@update');
Route::get('contract', 'ContractApi@index');
Route::get('contract/user', 'ContractApi@userContracts');
Route::get('contract/hicker', 'ContractApi@hickerContracts');
Route::post('contract/done', 'ContractApi@doneContracts');
Route::post('contract/cancel', 'ContractApi@cancelContracts');