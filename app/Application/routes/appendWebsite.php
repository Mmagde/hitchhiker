<?php

#### page control
Route::get('page/item/{id?}', 'PageController@show');
Route::post('page/item', 'PageController@store');
Route::post('page/item/{id}', 'PageController@update');
Route::get('page/{id}/delete', 'PageController@destroy');
#### page comment
Route::post('page/add/comment/{id}', 'PageCommentController@addComment');
Route::post('page/update/comment/{id}', 'PageCommentController@updateComment');
Route::get('page/delete/comment/{id}', 'PageCommentController@deleteComment');


#### categorie control
Route::get('categorie', 'CategorieController@index');
Route::get('categorie/item/{id?}', 'CategorieController@show');
Route::post('categorie/item', 'CategorieController@store');
Route::post('categorie/item/{id}', 'CategorieController@update');
Route::get('categorie/{id}/delete', 'CategorieController@destroy');
Route::get('categorie/{id}/view', 'CategorieController@getById');


























































































































































































































































































































































































































































#### city control
Route::get('city' , 'CityController@index');
Route::get('city/item/{id?}' , 'CityController@show');
Route::post('city/item' , 'CityController@store');
Route::post('city/item/{id}' , 'CityController@update');
Route::get('city/{id}/delete' , 'CityController@destroy');
Route::get('city/{id}/view' , 'CityController@getById');

#### type control
Route::get('type' , 'TypeController@index');
Route::get('type/item/{id?}' , 'TypeController@show');
Route::post('type/item' , 'TypeController@store');
Route::post('type/item/{id}' , 'TypeController@update');
Route::get('type/{id}/delete' , 'TypeController@destroy');
Route::get('type/{id}/view' , 'TypeController@getById');

#### requeststatus control
Route::get('requeststatus' , 'RequeststatusController@index');
Route::get('requeststatus/item/{id?}' , 'RequeststatusController@show');
Route::post('requeststatus/item' , 'RequeststatusController@store');
Route::post('requeststatus/item/{id}' , 'RequeststatusController@update');
Route::get('requeststatus/{id}/delete' , 'RequeststatusController@destroy');
Route::get('requeststatus/{id}/view' , 'RequeststatusController@getById');

#### contractstatus control
Route::get('contractstatus' , 'ContractstatusController@index');
Route::get('contractstatus/item/{id?}' , 'ContractstatusController@show');
Route::post('contractstatus/item' , 'ContractstatusController@store');
Route::post('contractstatus/item/{id}' , 'ContractstatusController@update');
Route::get('contractstatus/{id}/delete' , 'ContractstatusController@destroy');
Route::get('contractstatus/{id}/view' , 'ContractstatusController@getById');

#### trip control
Route::get('trip' , 'TripController@index');
Route::get('trip/item/{id?}' , 'TripController@show');
Route::post('trip/item' , 'TripController@store');
Route::post('trip/item/{id}' , 'TripController@update');
Route::get('trip/{id}/delete' , 'TripController@destroy');
Route::get('trip/{id}/view' , 'TripController@getById');

#### request control
Route::get('request' , 'RequestController@index');
Route::get('request/item/{id?}' , 'RequestController@show');
Route::post('request/item' , 'RequestController@store');
Route::post('request/item/{id}' , 'RequestController@update');
Route::get('request/{id}/delete' , 'RequestController@destroy');
Route::get('request/{id}/view' , 'RequestController@getById');

#### contract control
Route::get('contract' , 'ContractController@index');
Route::get('contract/item/{id?}' , 'ContractController@show');
Route::post('contract/item' , 'ContractController@store');
Route::post('contract/item/{id}' , 'ContractController@update');
Route::get('contract/{id}/delete' , 'ContractController@destroy');
Route::get('contract/{id}/view' , 'ContractController@getById');