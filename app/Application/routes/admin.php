<?php
Route::get('/', 'HomeController@index');
Route::get('icons', 'HomeController@icons');
Route::get('docs', 'HomeController@apiDocs');
Route::get('file-manager', 'HomeController@fileManager');
Route::get('theme/open-file', 'Themes\ThemeController@openFile');
Route::get('theme/{theme}', 'Themes\ThemeController@adminPanel');
Route::post('theme/save-file', 'Themes\ThemeController@save');

### commands
Route::get('commands', 'CommandsController@index');
Route::post('command/exe', 'CommandsController@exe');
Route::get('laravel/commands', 'CommandsController@command');
Route::post('command/otherExe', 'CommandsController@otherExe');
Route::post('laravel/haveCommand', 'CommandsController@haveCommand');
Route::get('exportImport', 'CommandsController@exportEmportModels');
Route::post('export', 'CommandsController@export');
Route::post('import', 'CommandsController@import');


### relations
Route::get('relation', 'RelationController@index');
Route::post('relation/exe', 'RelationController@exe');
Route::get('getCols/{model}', 'RelationController@getCols');
Route::post('relation/rollback', 'RelationController@rollback');

#### user control
Route::get('user', 'UserController@index');
Route::get('user/item/{id?}', 'UserController@show');
Route::post('user/item', 'UserController@store');
Route::post('user/item/{id}', 'UserController@update');
Route::get('user/{id}/delete', 'UserController@destroy');
Route::get('user/{id}/view', 'UserController@getById');
Route::get('user/pluck', 'UserController@pluck');


#### translation
Route::get('translation', 'TranslationController@index');
Route::get('translation/readFile/{file}', 'TranslationController@readFile');
Route::post('translation/save', 'TranslationController@save');
Route::get('translation/getAllContent/{file}', 'TranslationController@getAllContent');
Route::post('translation/both/save', 'TranslationController@bothSave');

#### permissions
Route::get('custom-permissions', 'Development\CustomPermissionsController@index');
Route::get('custom-permissions/readFile/{file}', 'Development\CustomPermissionsController@readFile');
Route::post('custom-permissions/save', 'Development\CustomPermissionsController@save');
Route::get('getControllerByType/{type}', 'Development\PermissionController@getControllerByType');
Route::get('getMethodByController/{controller}/{type}', 'Development\PermissionController@getMethodByController');


#### group control
Route::get('group', 'GroupController@index');
Route::get('group/item/{id?}', 'GroupController@show');
Route::post('group/item', 'GroupController@store');
Route::post('group/item/{id}', 'GroupController@update');
Route::get('group/{id}/delete', 'GroupController@destroy');
Route::get('group/{id}/view', 'GroupController@getById');
Route::get('group/pluck', 'GroupController@pluck');

#### role control
Route::get('role', 'RoleController@index');
Route::get('role/item/{id?}', 'RoleController@show');
Route::post('role/item', 'RoleController@store');
Route::post('role/item/{id}', 'RoleController@update');
Route::get('role/{id}/delete', 'RoleController@destroy');
Route::get('role/{id}/view', 'RoleController@getById');
Route::get('role/pluck', 'RoleController@pluck');
#### permission control
Route::get('permission', 'Development\PermissionController@index');
Route::get('permission/item/{id?}', 'Development\PermissionController@show');
Route::post('permission/item', 'Development\PermissionController@store');
Route::post('permission/item/{id}', 'Development\PermissionController@update');
Route::get('permission/{id}/delete', 'Development\PermissionController@destroy');
Route::get('permission/{id}/view', 'Development\PermissionController@getById');
Route::get('permission/pluck', 'PermissionController@pluck');
#### home control
Route::get('home/{pages?}/{limit?}', 'HomeController@index');
#### setting control
Route::get('setting', 'SettingController@index');
Route::get('setting/item/{id?}', 'SettingController@show');
Route::post('setting/item', 'SettingController@store');
Route::post('setting/item/{id}', 'SettingController@update');
Route::get('setting/{id}/delete', 'SettingController@destroy');
Route::get('setting/{id}/view', 'SettingController@getById');
Route::get('setting/pluck', 'SettingController@pluck');
#### menu control
Route::get('menu', 'MenuController@index');
Route::get('menu/item/{id?}', 'MenuController@show');
Route::post('menu/item', 'MenuController@store');
Route::post('menu/item/{id}', 'MenuController@update');
Route::get('menu/{id}/delete', 'MenuController@destroy');
Route::get('menu/{id}/view', 'MenuController@getById');
Route::post('update/menuItem', 'MenuController@menuItem');
Route::post('addNewItemToMenu', 'MenuController@addNewItemToMenu');
Route::get('deleteMenuItem/{id}', 'MenuController@deleteMenuItem');
Route::get('getItemInfo/{id}', 'MenuController@getItemInfo');
Route::post('updateOneMenuItem', 'MenuController@updateOneMenuItem');
Route::get('menu/pluck', 'MenuController@pluck');
#### log control
Route::get('log', 'LogController@index');
Route::get('log/item/{id?}', 'LogController@show');
Route::post('log/item', 'LogController@store');
Route::post('log/item/{id}', 'LogController@update');
Route::get('log/{id}/delete', 'LogController@destroy');
Route::get('log/{id}/view', 'LogController@getById');
Route::get('log/pluck', 'LogController@pluck');
#### contact control
Route::get('contact', 'ContactController@index');
Route::get('contact/item/{id?}', 'ContactController@show');
Route::post('contact/item', 'ContactController@store');
Route::post('contact/item/{id}', 'ContactController@update');
Route::get('contact/{id}/delete', 'ContactController@destroy');
Route::get('contact/{id}/view', 'ContactController@getById');
Route::post('contact/replay/{id}', 'ContactController@replayEmail');
Route::get('contact/pluck', 'ContactController@pluck');

#### page control
Route::get('page', 'PageController@index');
Route::get('page/item/{id?}', 'PageController@show');
Route::post('page/item', 'PageController@store');
Route::post('page/item/{id}', 'PageController@update');
Route::get('page/{id}/delete', 'PageController@destroy');
Route::get('page/{id}/view', 'PageController@getById');
Route::get('page/pluck', 'PageController@pluck');
#### page comment
Route::post('page/add/comment/{id}', 'PageCommentController@addComment');
Route::post('page/update/comment/{id}', 'PageCommentController@updateComment');
Route::get('page/delete/comment/{id}', 'PageCommentController@deleteComment');

#### categorie control
Route::get('categorie', 'CategorieController@index');
Route::get('categorie/item/{id?}', 'CategorieController@show');
Route::post('categorie/item', 'CategorieController@store');
Route::post('categorie/item/{id}', 'CategorieController@update');
Route::get('categorie/{id}/delete', 'CategorieController@destroy');
Route::get('categorie/{id}/view', 'CategorieController@getById');
Route::get('categorie/pluck', 'CategorieController@pluck');




























































































































































































































#### city control
Route::get('city' , 'CityController@index');
Route::get('city/item/{id?}' , 'CityController@show');
Route::post('city/item' , 'CityController@store');
Route::post('city/item/{id}' , 'CityController@update');
Route::get('city/{id}/delete' , 'CityController@destroy');
Route::get('city/{id}/view' , 'CityController@getById');
Route::get('city/pluck', 'CityController@pluck');
#### type control
Route::get('type' , 'TypeController@index');
Route::get('type/item/{id?}' , 'TypeController@show');
Route::post('type/item' , 'TypeController@store');
Route::post('type/item/{id}' , 'TypeController@update');
Route::get('type/{id}/delete' , 'TypeController@destroy');
Route::get('type/{id}/view' , 'TypeController@getById');
Route::get('type/pluck', 'TypeController@pluck');
#### requeststatus control
Route::get('requeststatus' , 'RequeststatusController@index');
Route::get('requeststatus/item/{id?}' , 'RequeststatusController@show');
Route::post('requeststatus/item' , 'RequeststatusController@store');
Route::post('requeststatus/item/{id}' , 'RequeststatusController@update');
Route::get('requeststatus/{id}/delete' , 'RequeststatusController@destroy');
Route::get('requeststatus/{id}/view' , 'RequeststatusController@getById');
Route::get('requeststatus/pluck', 'RequeststatusController@pluck');
#### contractstatus control
Route::get('contractstatus' , 'ContractstatusController@index');
Route::get('contractstatus/item/{id?}' , 'ContractstatusController@show');
Route::post('contractstatus/item' , 'ContractstatusController@store');
Route::post('contractstatus/item/{id}' , 'ContractstatusController@update');
Route::get('contractstatus/{id}/delete' , 'ContractstatusController@destroy');
Route::get('contractstatus/{id}/view' , 'ContractstatusController@getById');
Route::get('contractstatus/pluck', 'ContractstatusController@pluck');
#### trip control
Route::get('trip' , 'TripController@index');
Route::get('trip/item/{id?}' , 'TripController@show');
Route::post('trip/item' , 'TripController@store');
Route::post('trip/item/{id}' , 'TripController@update');
Route::get('trip/{id}/delete' , 'TripController@destroy');
Route::get('trip/{id}/view' , 'TripController@getById');
Route::get('trip/pluck', 'TripController@pluck');
#### request control
Route::get('request' , 'RequestController@index');
Route::get('request/item/{id?}' , 'RequestController@show');
Route::post('request/item' , 'RequestController@store');
Route::post('request/item/{id}' , 'RequestController@update');
Route::get('request/{id}/delete' , 'RequestController@destroy');
Route::get('request/{id}/view' , 'RequestController@getById');
Route::get('request/pluck', 'RequestController@pluck');
#### contract control
Route::get('contract' , 'ContractController@index');
Route::get('contract/item/{id?}' , 'ContractController@show');
Route::post('contract/item' , 'ContractController@store');
Route::post('contract/item/{id}' , 'ContractController@update');
Route::get('contract/{id}/delete' , 'ContractController@destroy');
Route::get('contract/{id}/view' , 'ContractController@getById');
Route::get('contract/pluck', 'ContractController@pluck');