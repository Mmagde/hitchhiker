@extends(layoutExtend('website'))

@section('title')
    {{ trans('type.type') }} {{ trans('home.view') }}
@endsection

@section('content')
<div class="pull-{{ getDirection() }} col-lg-9">
        <a href="{{ url('type') }}" class="btn btn-danger"><i class="fa fa-arrow-left"></i> {{ trans('website.Back') }}  </a>
		 <table class="table table-bordered  table-striped" > 
				<tr>
				<th width="200">{{ trans("type.name") }}</th>
					<td>{{ nl2br($item->name) }}</td>
				</tr>
		</table>

        @include('website.type.buttons.delete' , ['id' => $item->id])
        @include('website.type.buttons.edit' , ['id' => $item->id])
</div>
@endsection
