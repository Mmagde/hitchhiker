@extends(layoutExtend('website'))

@section('title')
    {{ trans('city.city') }} {{ trans('home.view') }}
@endsection

@section('content')
<div class="pull-{{ getDirection() }} col-lg-9">
        <a href="{{ url('city') }}" class="btn btn-danger"><i class="fa fa-arrow-left"></i> {{ trans('website.Back') }}  </a>
		 <table class="table table-bordered  table-striped" > 
				<tr>
				<th width="200">{{ trans("city.name") }}</th>
					<td>{{ nl2br($item->name) }}</td>
				</tr>
		</table>

        @include('website.city.buttons.delete' , ['id' => $item->id])
        @include('website.city.buttons.edit' , ['id' => $item->id])
</div>
@endsection
