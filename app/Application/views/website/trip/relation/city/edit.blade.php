		<div class="form-group {{ $errors->has("city") ? "has-error" : "" }}">
			<label for="city">{{ trans( "city.city") }}</label>
			@php $cities = App\Application\Model\City::pluck("name" ,"id")->all()  @endphp
			@php  $city_id = isset($item) ? $item->city_id : null @endphp
			<select name="city_id"  class="form-control" >
			@foreach( $cities as $key => $relatedItem)
			<option value="{{ $key }}"  {{ $key == $city_id  ? "selected" : "" }}> {{ is_json($relatedItem) ? getDefaultValueKey($relatedItem) :  $relatedItem}}</option>
			@endforeach
			</select>
			@if ($errors->has("city"))
				<div class="alert alert-danger">
					<span class="help-block">
						<strong>{{ $errors->first("city") }}</strong>
					</span>
				</div>
			@endif
			</div>
