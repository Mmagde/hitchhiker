@extends(layoutExtend('website'))
 @section('title')
    {{ trans('trip.trip') }} {{ trans('home.view') }}
@endsection
 @section('content')
<div class="pull-{{ getDirection() }} col-lg-9">
        <a href="{{ url('trip') }}" class="btn btn-danger"><i class="fa fa-arrow-left"></i> {{ trans('website.Back') }}  </a>
   <table class="table table-bordered  table-striped" > 
    <tr>
    <th width="200">{{ trans("trip.phone") }}</th>
     <td>{{ nl2br($item->phone) }}</td>
    </tr>
    <tr>
    <th width="200">{{ trans("trip.date") }}</th>
     <td>{{ nl2br($item->date) }}</td>
    </tr>
    <tr>
    <th width="200">{{ trans("trip.price") }}</th>
     <td>{{ nl2br($item->price) }}</td>
    </tr>
  </table>
         @include('website.trip.buttons.delete' , ['id' => $item->id])
        @include('website.trip.buttons.edit' , ['id' => $item->id])
</div>
@endsection
