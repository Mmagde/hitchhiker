@extends(layoutExtend('website'))

@section('title')
     {{ trans('trip.trip') }} {{ trans('home.control') }}
@endsection

@section('content')
 <div class="pull-{{ getDirection() }} col-lg-9">
    <div><h1>{{ trans('website.trip') }}</h1></div>
     <div><a href="{{ url('trip/item') }}" class="btn btn-default"><i class="fa fa-plus"></i> {{ trans('website.trip') }}</a><br></div>
 	<form method="get" class="form-inline">
		<div class="form-group">
			<input type="text" name="from" class="form-control datepicker2" placeholder="{{ trans("admin.from") }}"value="{{ request()->has("from") ? request()->get("from") : "" }}">
		 </div>
		<div class="form-group">
			<input type="text" name="to" class="form-control datepicker2" placeholder="{{ trans("admin.to") }}"value="{{ request()->has("to") ? request()->get("to") : "" }}">
		</div>
		<div class="form-group"> 
			<input type="text" name="phone" class="form-control " placeholder="{{ trans("trip.phone") }}" value="{{ request()->has("phone") ? request()->get("phone") : "" }}"> 
		</div> 
		<div class="form-group"> 
			<input type="text" name="date" class="form-control datepicker2" placeholder="{{ trans("trip.date") }}" value="{{ request()->has("date") ? request()->get("date") : "" }}"> 
		</div> 
		<div class="form-group"> 
			<input type="text" name="price" class="form-control " placeholder="{{ trans("trip.price") }}" value="{{ request()->has("price") ? request()->get("price") : "" }}"> 
		</div> 
		 <button class="btn btn-success" type="submit" ><i class="fa fa-search" ></i ></button>
		<a href="{{ url("trip") }}" class="btn btn-danger" ><i class="fa fa-close" ></i></a>
	 </form > 
<br ><table class="table table-responsive table-striped table-bordered"> 
		<thead > 
			<tr> 
				<th>{{ trans("trip.phone") }}</th> 
				<th>{{ trans("trip.edit") }}</th> 
				<th>{{ trans("trip.show") }}</th> 
				<th>{{
            trans("trip.delete") }}</th> 
				</thead > 
		<tbody > 
		@if (count($items) > 0) 
			@foreach ($items as $d) 
				 <tr>
					<td>{{ str_limit($d->phone , 20) }}</td> 
				<td> @include("website.trip.buttons.edit", ["id" => $d->id])</td> 
					<td> @include("website.trip.buttons.view", ["id" => $d->id])</td> 
					<td> @include("website.trip.buttons.delete", ["id" => $d->id])</td> 
					</tr> 
					@endforeach
				@endif
			 </tbody > 
		</table > 
	@include(layoutPaginate() , ["items" => $items])
		
</div>
@endsection
