<h2>{{ ucfirst(trans('admin.Latest'))}} {{ ucfirst('city') }}</h2>
<hr>
@php $sidebarCity = \App\Application\Model\City::orderBy("id", "DESC")->limit(5)->get(); @endphp
		@if (count($sidebarCity) > 0)
			@foreach ($sidebarCity as $d)
				 <div>
					<p><a href="{{ url("city/".$d->id."/view") }}">{{ str_limit($d->name , 20) }}</a></p > 
					<p><a href="{{ url("city/".$d->id."/view") }}" ><i class="fa fa-eye" ></i ></a> <small ><i class="fa fa-calendar-o" ></i > {{ $d->created_at }}</small ></p > 
				<hr > 
				</div> 
			@endforeach
		@endif
			