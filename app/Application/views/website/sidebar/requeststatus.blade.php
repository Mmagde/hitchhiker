<h2>{{ ucfirst(trans('admin.Latest'))}} {{ ucfirst('requeststatus') }}</h2>
<hr>
@php $sidebarRequeststatus = \App\Application\Model\Requeststatus::orderBy("id", "DESC")->limit(5)->get(); @endphp
		@if (count($sidebarRequeststatus) > 0)
			@foreach ($sidebarRequeststatus as $d)
				 <div>
					<p><a href="{{ url("requeststatus/".$d->id."/view") }}">{{ str_limit($d->name , 20) }}</a></p > 
					<p><a href="{{ url("requeststatus/".$d->id."/view") }}" ><i class="fa fa-eye" ></i ></a> <small ><i class="fa fa-calendar-o" ></i > {{ $d->created_at }}</small ></p > 
				<hr > 
				</div> 
			@endforeach
		@endif
			