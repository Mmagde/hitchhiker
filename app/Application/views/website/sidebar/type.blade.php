<h2>{{ ucfirst(trans('admin.Latest'))}} {{ ucfirst('type') }}</h2>
<hr>
@php $sidebarType = \App\Application\Model\Type::orderBy("id", "DESC")->limit(5)->get(); @endphp
		@if (count($sidebarType) > 0)
			@foreach ($sidebarType as $d)
				 <div>
					<p><a href="{{ url("type/".$d->id."/view") }}">{{ str_limit($d->name , 20) }}</a></p > 
					<p><a href="{{ url("type/".$d->id."/view") }}" ><i class="fa fa-eye" ></i ></a> <small ><i class="fa fa-calendar-o" ></i > {{ $d->created_at }}</small ></p > 
				<hr > 
				</div> 
			@endforeach
		@endif
			