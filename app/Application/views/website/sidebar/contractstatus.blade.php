<h2>{{ ucfirst(trans('admin.Latest'))}} {{ ucfirst('contractstatus') }}</h2>
<hr>
@php $sidebarContractstatus = \App\Application\Model\Contractstatus::orderBy("id", "DESC")->limit(5)->get(); @endphp
		@if (count($sidebarContractstatus) > 0)
			@foreach ($sidebarContractstatus as $d)
				 <div>
					<p><a href="{{ url("contractstatus/".$d->id."/view") }}">{{ str_limit($d->name , 20) }}</a></p > 
					<p><a href="{{ url("contractstatus/".$d->id."/view") }}" ><i class="fa fa-eye" ></i ></a> <small ><i class="fa fa-calendar-o" ></i > {{ $d->created_at }}</small ></p > 
				<hr > 
				</div> 
			@endforeach
		@endif
			