@extends(layoutExtend('website'))
 @section('title')
    {{ trans('contract.contract') }} {{ trans('home.view') }}
@endsection
 @section('content')
<div class="pull-{{ getDirection() }} col-lg-9">
        <a href="{{ url('contract') }}" class="btn btn-danger"><i class="fa fa-arrow-left"></i> {{ trans('website.Back') }}  </a>
   <table class="table table-bordered  table-striped" > 
    <tr>
    <th width="200">{{ trans("contract.price") }}</th>
     <td>{{ nl2br($item->price) }}</td>
    </tr>
    <tr>
    <th width="200">{{ trans("contract.status") }}</th>
     <td>{{ nl2br($item->status) }}</td>
    </tr>
  </table>
         @include('website.contract.buttons.delete' , ['id' => $item->id])
        @include('website.contract.buttons.edit' , ['id' => $item->id])
</div>
@endsection
