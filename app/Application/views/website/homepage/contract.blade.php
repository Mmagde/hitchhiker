<h2>{{ ucfirst(trans('admin.Random'))}} {{ ucfirst('contract') }}</h2>
<hr>
@php $sidebarContract = \App\Application\Model\Contract::inRandomOrder()->limit(5)->get(); @endphp
		@if (count($sidebarContract) > 0)
			@foreach ($sidebarContract as $d)
				 <div>
					<h2 > {{ str_limit($d->price , 50) }}</h2 > 
					<p> {{ str_limit($d->status , 300) }}</p > 
					 <p><a href="{{ url("contract/".$d->id."/view") }}" ><i class="fa fa-eye" ></i ></a> <small ><i class="fa fa-calendar-o" ></i > {{ $d->created_at }}</small ></p > 
				<hr > 
				</div> 
			@endforeach
		@endif
			