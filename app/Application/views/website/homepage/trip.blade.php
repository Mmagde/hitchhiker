<h2>{{ ucfirst(trans('admin.Random'))}} {{ ucfirst('trip') }}</h2>
<hr>
@php $sidebarTrip = \App\Application\Model\Trip::inRandomOrder()->limit(5)->get(); @endphp
		@if (count($sidebarTrip) > 0)
			@foreach ($sidebarTrip as $d)
				 <div>
					<h2 > {{ str_limit($d->phone , 50) }}</h2 > 
					<p> {{ str_limit($d->date , 300) }}</p > 
					<p> {{ str_limit($d->price , 300) }}</p > 
					 <p><a href="{{ url("trip/".$d->id."/view") }}" ><i class="fa fa-eye" ></i ></a> <small ><i class="fa fa-calendar-o" ></i > {{ $d->created_at }}</small ></p > 
				<hr > 
				</div> 
			@endforeach
		@endif
			