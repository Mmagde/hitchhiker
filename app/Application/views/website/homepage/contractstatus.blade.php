<h2>{{ ucfirst(trans('admin.Random'))}} {{ ucfirst('contractstatus') }}</h2>
<hr>
@php $sidebarContractstatus = \App\Application\Model\Contractstatus::inRandomOrder()->limit(5)->get(); @endphp
		@if (count($sidebarContractstatus) > 0)
			@foreach ($sidebarContractstatus as $d)
				 <div>
					<h2 > {{ str_limit($d->name , 50) }}</h2 > 
					 <p><a href="{{ url("contractstatus/".$d->id."/view") }}" ><i class="fa fa-eye" ></i ></a> <small ><i class="fa fa-calendar-o" ></i > {{ $d->created_at }}</small ></p > 
				<hr > 
				</div> 
			@endforeach
		@endif
			