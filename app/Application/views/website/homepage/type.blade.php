<h2>{{ ucfirst(trans('admin.Random'))}} {{ ucfirst('type') }}</h2>
<hr>
@php $sidebarType = \App\Application\Model\Type::inRandomOrder()->limit(5)->get(); @endphp
		@if (count($sidebarType) > 0)
			@foreach ($sidebarType as $d)
				 <div>
					<h2 > {{ str_limit($d->name , 50) }}</h2 > 
					 <p><a href="{{ url("type/".$d->id."/view") }}" ><i class="fa fa-eye" ></i ></a> <small ><i class="fa fa-calendar-o" ></i > {{ $d->created_at }}</small ></p > 
				<hr > 
				</div> 
			@endforeach
		@endif
			