<h2>{{ ucfirst(trans('admin.Random'))}} {{ ucfirst('requeststatus') }}</h2>
<hr>
@php $sidebarRequeststatus = \App\Application\Model\Requeststatus::inRandomOrder()->limit(5)->get(); @endphp
		@if (count($sidebarRequeststatus) > 0)
			@foreach ($sidebarRequeststatus as $d)
				 <div>
					<h2 > {{ str_limit($d->name , 50) }}</h2 > 
					 <p><a href="{{ url("requeststatus/".$d->id."/view") }}" ><i class="fa fa-eye" ></i ></a> <small ><i class="fa fa-calendar-o" ></i > {{ $d->created_at }}</small ></p > 
				<hr > 
				</div> 
			@endforeach
		@endif
			