		<div class="form-group {{ $errors->has("requeststatus") ? "has-error" : "" }}">
			<label for="requeststatus">{{ trans( "requeststatus.requeststatus") }}</label>
			@php $requeststatuses = App\Application\Model\Requeststatus::pluck("name" ,"id")->all()  @endphp
			@php  $requeststatus_id = isset($item) ? $item->requeststatus_id : null @endphp
			<select name="requeststatus_id"  class="form-control" >
			@foreach( $requeststatuses as $key => $relatedItem)
			<option value="{{ $key }}"  {{ $key == $requeststatus_id  ? "selected" : "" }}> {{ is_json($relatedItem) ? getDefaultValueKey($relatedItem) :  $relatedItem}}</option>
			@endforeach
			</select>
			@if ($errors->has("requeststatus"))
				<div class="alert alert-danger">
					<span class="help-block">
						<strong>{{ $errors->first("requeststatus") }}</strong>
					</span>
				</div>
			@endif
			</div>
