<span onclick="deleteThisItem(this)" data-link="{{ url('admin/trip/'.$id.'/delete') }}" class="btn bg-deep-purple btn-circle waves-effect waves-circle waves-float" >
    <i class="material-icons">delete</i>
</span>
