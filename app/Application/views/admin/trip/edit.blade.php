@extends(layoutExtend())
 @section('title')
    {{ trans('trip.trip') }} {{  isset($item) ? trans('home.edit')  : trans('home.add')  }}
@endsection
 @section('content')
    @component(layoutForm() , ['title' => trans('trip.trip') , 'model' => 'trip' , 'action' => isset($item) ? trans('home.edit')  : trans('home.add')  ])
         @include(layoutMessage())
        <form action="{{ concatenateLangToUrl('admin/trip/item') }}{{ isset($item) ? '/'.$item->id : '' }}" method="post" enctype="multipart/form-data">
            {{ csrf_field() }}
            @include("admin.trip.relation.from.edit")
            @include("admin.trip.relation.to.edit")
            @include("admin.trip.relation.user.edit")
     <div class="form-group {{ $errors->has("phone") ? "has-error" : "" }}" > 
   <label for="phone">{{ trans("trip.phone")}}</label>
    <input type="text" name="phone" class="form-control" id="phone" value="{{ isset($item->phone) ? $item->phone : old("phone") }}"  placeholder="{{ trans("trip.phone")}}">
  </div>
   @if ($errors->has("phone"))
    <div class="alert alert-danger">
     <span class='help-block'>
      <strong>{{ $errors->first("phone") }}</strong>
     </span>
    </div>
   @endif
   <div class="form-group {{ $errors->has("date") ? "has-error" : "" }}" > 
   <label for="date">{{ trans("trip.date")}}</label>
     <input type="text" name="date" class="form-control datepicker2" id="date" value="{{ isset($item->date) ? $item->date : old("date") }}"  placeholder="{{ trans("trip.date")}}" > 
  </div>
   @if ($errors->has("date"))
    <div class="alert alert-danger">
     <span class='help-block'>
      <strong>{{ $errors->first("date") }}</strong>
     </span>
    </div>
   @endif
   <div class="form-group {{ $errors->has("price") ? "has-error" : "" }}" > 
   <label for="price">{{ trans("trip.price")}}</label>
    <input type="text" name="price" class="form-control" id="price" value="{{ isset($item->price) ? $item->price : old("price") }}"  placeholder="{{ trans("trip.price")}}">
  </div>
   @if ($errors->has("price"))
    <div class="alert alert-danger">
     <span class='help-block'>
      <strong>{{ $errors->first("price") }}</strong>
     </span>
    </div>
   @endif
              <div class="form-group">
                <button type="submit" name="submit" class="btn btn-default" >
                    <i class="material-icons">check_circle</i>
                    {{ trans('home.save') }}  {{ trans('trip.trip') }}
                </button>
            </div>
        </form>
    @endcomponent
@endsection
