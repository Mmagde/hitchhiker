@extends(layoutExtend())
 @section('title')
    {{ trans('trip.trip') }} {{ trans('home.view') }}
@endsection
 @section('content')
    @component(layoutForm() , ['title' => trans('trip.trip') , 'model' => 'trip' , 'action' => trans('home.view')  ])
   <table class="table table-bordered  table-striped" > 
    <tr>
    <th width="200">{{ trans("trip.phone") }}</th>
     <td>{{ nl2br($item->phone) }}</td>
    </tr>
    <tr>
    <th width="200">{{ trans("trip.date") }}</th>
     <td>{{ nl2br($item->date) }}</td>
    </tr>
    <tr>
    <th width="200">{{ trans("trip.price") }}</th>
     <td>{{ nl2br($item->price) }}</td>
    </tr>
  </table>
         @include('admin.trip.buttons.delete' , ['id' => $item->id])
        @include('admin.trip.buttons.edit' , ['id' => $item->id])
    @endcomponent
@endsection
