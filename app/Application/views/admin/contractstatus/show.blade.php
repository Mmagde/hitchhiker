@extends(layoutExtend())

@section('title')
    {{ trans('contractstatus.contractstatus') }} {{ trans('home.view') }}
@endsection

@section('content')
    @component(layoutForm() , ['title' => trans('contractstatus.contractstatus') , 'model' => 'contractstatus' , 'action' => trans('home.view')  ])
		 <table class="table table-bordered  table-striped" > 
				<tr>
				<th width="200">{{ trans("contractstatus.name") }}</th>
					<td>{{ nl2br($item->name) }}</td>
				</tr>
		</table>

        @include('admin.contractstatus.buttons.delete' , ['id' => $item->id])
        @include('admin.contractstatus.buttons.edit' , ['id' => $item->id])
    @endcomponent
@endsection
