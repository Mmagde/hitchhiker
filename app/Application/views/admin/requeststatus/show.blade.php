@extends(layoutExtend())

@section('title')
    {{ trans('requeststatus.requeststatus') }} {{ trans('home.view') }}
@endsection

@section('content')
    @component(layoutForm() , ['title' => trans('requeststatus.requeststatus') , 'model' => 'requeststatus' , 'action' => trans('home.view')  ])
		 <table class="table table-bordered  table-striped" > 
				<tr>
				<th width="200">{{ trans("requeststatus.name") }}</th>
					<td>{{ nl2br($item->name) }}</td>
				</tr>
		</table>

        @include('admin.requeststatus.buttons.delete' , ['id' => $item->id])
        @include('admin.requeststatus.buttons.edit' , ['id' => $item->id])
    @endcomponent
@endsection
