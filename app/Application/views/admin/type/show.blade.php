@extends(layoutExtend())

@section('title')
    {{ trans('type.type') }} {{ trans('home.view') }}
@endsection

@section('content')
    @component(layoutForm() , ['title' => trans('type.type') , 'model' => 'type' , 'action' => trans('home.view')  ])
		 <table class="table table-bordered  table-striped" > 
				<tr>
				<th width="200">{{ trans("type.name") }}</th>
					<td>{{ nl2br($item->name) }}</td>
				</tr>
		</table>

        @include('admin.type.buttons.delete' , ['id' => $item->id])
        @include('admin.type.buttons.edit' , ['id' => $item->id])
    @endcomponent
@endsection
