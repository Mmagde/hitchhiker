@extends(layoutExtend())
 @section('title')
    {{ trans('contract.contract') }} {{ trans('home.view') }}
@endsection
 @section('content')
    @component(layoutForm() , ['title' => trans('contract.contract') , 'model' => 'contract' , 'action' => trans('home.view')  ])
   <table class="table table-bordered  table-striped" > 
    <tr>
    <th width="200">{{ trans("contract.price") }}</th>
     <td>{{ nl2br($item->price) }}</td>
    </tr>
    <tr>
    <th width="200">{{ trans("contract.status") }}</th>
     <td>{{ nl2br($item->status) }}</td>
    </tr>
  </table>
         @include('admin.contract.buttons.delete' , ['id' => $item->id])
        @include('admin.contract.buttons.edit' , ['id' => $item->id])
    @endcomponent
@endsection
