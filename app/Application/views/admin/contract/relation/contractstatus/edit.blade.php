		<div class="form-group {{ $errors->has("contractstatus") ? "has-error" : "" }}">
			<label for="contractstatus">{{ trans( "contractstatus.contractstatus") }}</label>
			@php $contractstatuses = App\Application\Model\Contractstatus::pluck("name" ,"id")->all()  @endphp
			@php  $contractstatus_id = isset($item) ? $item->contractstatus_id : null @endphp
			<select name="contractstatus_id"  class="form-control" >
			@foreach( $contractstatuses as $key => $relatedItem)
			<option value="{{ $key }}"  {{ $key == $contractstatus_id  ? "selected" : "" }}> {{ is_json($relatedItem) ? getDefaultValueKey($relatedItem) :  $relatedItem}}</option>
			@endforeach
			</select>
			@if ($errors->has("contractstatus"))
				<div class="alert alert-danger">
					<span class="help-block">
						<strong>{{ $errors->first("contractstatus") }}</strong>
					</span>
				</div>
			@endif
			</div>
