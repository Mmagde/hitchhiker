		<tr>
			<th>
			{{ trans( "contractstatus.contractstatus") }}
			</th>
			<td>
				@php $contractstatus = App\Application\Model\Contractstatus::find($item->contractstatus_id);  @endphp
				{{ is_json($contractstatus->name) ? getDefaultValueKey($contractstatus->name) :  $contractstatus->name}}
			</td>
		</tr>
