<div class="form-group {{ $errors->has(" to_id ") ? "has-error " : " " }}">
	<label for="To">{{ trans( "city.to") }}</label> @php $cities = App\Application\Model\City::pluck("name" ,"id")->all() 
@endphp
	@php $city_id = isset($item) ? $item->from_id : null 
@endphp
	<select name="to_id" class="form-control">
			@foreach( $cities as $key => $relatedItem)
			<option value="{{ $key }}"  {{ $key == $city_id  ? "selected" : "" }}> {{ is_json($relatedItem) ? getDefaultValueKey($relatedItem) :  $relatedItem}}</option>
			@endforeach
			</select> @if ($errors->has("to_id"))
	<div class="alert alert-danger">
		<span class="help-block">
						<strong>{{ $errors->first("to_id") }}</strong>
					</span>
	</div>
	@endif
</div>