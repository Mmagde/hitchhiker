		<div class="form-group {{ $errors->has("hicker_id") ? "has-error" : "" }}">
			<label for="hicker">{{ trans( "user.hicker") }}</label>
			@php $users = App\Application\Model\User::pluck("name" ,"id")->all()  @endphp
			@php  $user_id = isset($item) ? $item->hicker_id : null @endphp
			<select name="hicker_id"  class="form-control" >
			@foreach( $users as $key => $relatedItem)
			<option value="{{ $key }}"  {{ $key == $user_id  ? "selected" : "" }}> {{ is_json($relatedItem) ? getDefaultValueKey($relatedItem) :  $relatedItem}}</option>
			@endforeach
			</select>
			@if ($errors->has("hicker_id"))
				<div class="alert alert-danger">
					<span class="help-block">
						<strong>{{ $errors->first("hicker_id") }}</strong>
					</span>
				</div>
			@endif
			</div>
