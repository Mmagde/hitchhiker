@extends(layoutExtend())

@section('title')
    {{ trans('city.city') }} {{ trans('home.view') }}
@endsection

@section('content')
    @component(layoutForm() , ['title' => trans('city.city') , 'model' => 'city' , 'action' => trans('home.view')  ])
		 <table class="table table-bordered  table-striped" > 
				<tr>
				<th width="200">{{ trans("city.name") }}</th>
					<td>{{ nl2br($item->name) }}</td>
				</tr>
		</table>

        @include('admin.city.buttons.delete' , ['id' => $item->id])
        @include('admin.city.buttons.edit' , ['id' => $item->id])
    @endcomponent
@endsection
