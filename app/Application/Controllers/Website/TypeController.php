<?php

namespace App\Application\Controllers\Website;

use App\Application\Controllers\AbstractController;
use Alert;
use App\Application\Model\Type;
use App\Application\Requests\Website\Type\AddRequestType;
use App\Application\Requests\Website\Type\UpdateRequestType;

class TypeController extends AbstractController
{

     public function __construct(Type $model)
     {
        parent::__construct($model);
     }

     public function index(){
        $items = $this->model;

        if(request()->has('from') && request()->get('from') != ''){
            $items = $items->whereDate('created_at' , '>=' , request()->get('from'));
        }

        if(request()->has('to') && request()->get('to') != ''){
            $items = $items->whereDate('created_at' , '<=' , request()->get('to'));
        }

			if(request()->has("name") && request()->get("name") != ""){
				$items = $items->where("name","=", request()->get("name"));
			}



        $items = $items->paginate(env('PAGINATE'));
        return view('website.type.index' , compact('items'));
     }

     public function show($id = null){
         return $this->createOrEdit('website.type.edit' , $id);
     }

     public function store(AddRequestType $request){
          $item =  $this->storeOrUpdate($request , null , true);
          return redirect('type');
     }

     public function update($id , UpdateRequestType $request){
          $item = $this->storeOrUpdate($request, $id, true);
return redirect()->back();

     }

     public function getById($id){
         $fields = $this->model->findOrFail($id);
         return $this->createOrEdit('website.type.show' , $id , ['fields' =>  $fields]);
     }

     public function destroy($id){
         return $this->deleteItem($id , 'type')->with('sucess' , 'Done Delete Type From system');
     }


}
