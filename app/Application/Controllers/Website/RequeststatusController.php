<?php

namespace App\Application\Controllers\Website;

use App\Application\Controllers\AbstractController;
use Alert;
use App\Application\Model\Requeststatus;
use App\Application\Requests\Website\Requeststatus\AddRequestRequeststatus;
use App\Application\Requests\Website\Requeststatus\UpdateRequestRequeststatus;

class RequeststatusController extends AbstractController
{

     public function __construct(Requeststatus $model)
     {
        parent::__construct($model);
     }

     public function index(){
        $items = $this->model;

        if(request()->has('from') && request()->get('from') != ''){
            $items = $items->whereDate('created_at' , '>=' , request()->get('from'));
        }

        if(request()->has('to') && request()->get('to') != ''){
            $items = $items->whereDate('created_at' , '<=' , request()->get('to'));
        }

			if(request()->has("name") && request()->get("name") != ""){
				$items = $items->where("name","=", request()->get("name"));
			}



        $items = $items->paginate(env('PAGINATE'));
        return view('website.requeststatus.index' , compact('items'));
     }

     public function show($id = null){
         return $this->createOrEdit('website.requeststatus.edit' , $id);
     }

     public function store(AddRequestRequeststatus $request){
          $item =  $this->storeOrUpdate($request , null , true);
          return redirect('requeststatus');
     }

     public function update($id , UpdateRequestRequeststatus $request){
          $item = $this->storeOrUpdate($request, $id, true);
return redirect()->back();

     }

     public function getById($id){
         $fields = $this->model->findOrFail($id);
         return $this->createOrEdit('website.requeststatus.show' , $id , ['fields' =>  $fields]);
     }

     public function destroy($id){
         return $this->deleteItem($id , 'requeststatus')->with('sucess' , 'Done Delete Requeststatus From system');
     }


}
