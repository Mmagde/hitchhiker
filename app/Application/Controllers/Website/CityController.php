<?php

namespace App\Application\Controllers\Website;

use App\Application\Controllers\AbstractController;
use Alert;
use App\Application\Model\City;
use App\Application\Requests\Website\City\AddRequestCity;
use App\Application\Requests\Website\City\UpdateRequestCity;

class CityController extends AbstractController
{

     public function __construct(City $model)
     {
        parent::__construct($model);
     }

     public function index(){
        $items = $this->model;

        if(request()->has('from') && request()->get('from') != ''){
            $items = $items->whereDate('created_at' , '>=' , request()->get('from'));
        }

        if(request()->has('to') && request()->get('to') != ''){
            $items = $items->whereDate('created_at' , '<=' , request()->get('to'));
        }

			if(request()->has("name") && request()->get("name") != ""){
				$items = $items->where("name","=", request()->get("name"));
			}



        $items = $items->paginate(env('PAGINATE'));
        return view('website.city.index' , compact('items'));
     }

     public function show($id = null){
         return $this->createOrEdit('website.city.edit' , $id);
     }

     public function store(AddRequestCity $request){
          $item =  $this->storeOrUpdate($request , null , true);
          return redirect('city');
     }

     public function update($id , UpdateRequestCity $request){
          $item = $this->storeOrUpdate($request, $id, true);
return redirect()->back();

     }

     public function getById($id){
         $fields = $this->model->findOrFail($id);
         return $this->createOrEdit('website.city.show' , $id , ['fields' =>  $fields]);
     }

     public function destroy($id){
         return $this->deleteItem($id , 'city')->with('sucess' , 'Done Delete City From system');
     }


}
