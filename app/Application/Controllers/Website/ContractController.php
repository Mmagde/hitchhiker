<?php

namespace App\Application\Controllers\Website;

use App\Application\Controllers\AbstractController;
use Alert;
use App\Application\Model\Contract;
use App\Application\Requests\Website\Contract\AddRequestContract;
use App\Application\Requests\Website\Contract\UpdateRequestContract;

class ContractController extends AbstractController
{

     public function __construct(Contract $model)
     {
        parent::__construct($model);
     }

     public function index(){
        $items = $this->model;

        if(request()->has('from') && request()->get('from') != ''){
            $items = $items->whereDate('created_at' , '>=' , request()->get('from'));
        }

        if(request()->has('to') && request()->get('to') != ''){
            $items = $items->whereDate('created_at' , '<=' , request()->get('to'));
        }

			if(request()->has("price") && request()->get("price") != ""){
				$items = $items->where("price","=", request()->get("price"));
			}

			if(request()->has("status") && request()->get("status") != ""){
				$items = $items->where("status","=", request()->get("status"));
			}



        $items = $items->paginate(env('PAGINATE'));
        return view('website.contract.index' , compact('items'));
     }

     public function show($id = null){
         return $this->createOrEdit('website.contract.edit' , $id);
     }

     public function store(AddRequestContract $request){
          $item =  $this->storeOrUpdate($request , null , true);
          return redirect('contract');
     }

     public function update($id , UpdateRequestContract $request){
          $item = $this->storeOrUpdate($request, $id, true);
return redirect()->back();

     }

     public function getById($id){
         $fields = $this->model->findOrFail($id);
         return $this->createOrEdit('website.contract.show' , $id , ['fields' =>  $fields]);
     }

     public function destroy($id){
         return $this->deleteItem($id , 'contract')->with('sucess' , 'Done Delete Contract From system');
     }


}
