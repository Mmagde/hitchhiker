<?php

namespace App\Application\Controllers\Website;

use App\Application\Controllers\AbstractController;
use Alert;
use App\Application\Model\Contractstatus;
use App\Application\Requests\Website\Contractstatus\AddRequestContractstatus;
use App\Application\Requests\Website\Contractstatus\UpdateRequestContractstatus;

class ContractstatusController extends AbstractController
{

     public function __construct(Contractstatus $model)
     {
        parent::__construct($model);
     }

     public function index(){
        $items = $this->model;

        if(request()->has('from') && request()->get('from') != ''){
            $items = $items->whereDate('created_at' , '>=' , request()->get('from'));
        }

        if(request()->has('to') && request()->get('to') != ''){
            $items = $items->whereDate('created_at' , '<=' , request()->get('to'));
        }

			if(request()->has("name") && request()->get("name") != ""){
				$items = $items->where("name","=", request()->get("name"));
			}



        $items = $items->paginate(env('PAGINATE'));
        return view('website.contractstatus.index' , compact('items'));
     }

     public function show($id = null){
         return $this->createOrEdit('website.contractstatus.edit' , $id);
     }

     public function store(AddRequestContractstatus $request){
          $item =  $this->storeOrUpdate($request , null , true);
          return redirect('contractstatus');
     }

     public function update($id , UpdateRequestContractstatus $request){
          $item = $this->storeOrUpdate($request, $id, true);
return redirect()->back();

     }

     public function getById($id){
         $fields = $this->model->findOrFail($id);
         return $this->createOrEdit('website.contractstatus.show' , $id , ['fields' =>  $fields]);
     }

     public function destroy($id){
         return $this->deleteItem($id , 'contractstatus')->with('sucess' , 'Done Delete Contractstatus From system');
     }


}
