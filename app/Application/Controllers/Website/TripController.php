<?php

namespace App\Application\Controllers\Website;

use App\Application\Controllers\AbstractController;
use Alert;
use App\Application\Model\Trip;
use App\Application\Requests\Website\Trip\AddRequestTrip;
use App\Application\Requests\Website\Trip\UpdateRequestTrip;

class TripController extends AbstractController
{

     public function __construct(Trip $model)
     {
        parent::__construct($model);
     }

     public function index(){
        $items = $this->model;

        if(request()->has('from') && request()->get('from') != ''){
            $items = $items->whereDate('created_at' , '>=' , request()->get('from'));
        }

        if(request()->has('to') && request()->get('to') != ''){
            $items = $items->whereDate('created_at' , '<=' , request()->get('to'));
        }

			if(request()->has("phone") && request()->get("phone") != ""){
				$items = $items->where("phone","=", request()->get("phone"));
			}

			if(request()->has("date") && request()->get("date") != ""){
				$items = $items->where("date","=", request()->get("date"));
			}

			if(request()->has("price") && request()->get("price") != ""){
				$items = $items->where("price","=", request()->get("price"));
			}



        $items = $items->paginate(env('PAGINATE'));
        return view('website.trip.index' , compact('items'));
     }

     public function show($id = null){
         return $this->createOrEdit('website.trip.edit' , $id);
     }

     public function store(AddRequestTrip $request){
          $item =  $this->storeOrUpdate($request , null , true);
          return redirect('trip');
     }

     public function update($id , UpdateRequestTrip $request){
          $item = $this->storeOrUpdate($request, $id, true);
return redirect()->back();

     }

     public function getById($id){
         $fields = $this->model->findOrFail($id);
         return $this->createOrEdit('website.trip.show' , $id , ['fields' =>  $fields]);
     }

     public function destroy($id){
         return $this->deleteItem($id , 'trip')->with('sucess' , 'Done Delete Trip From system');
     }


}
