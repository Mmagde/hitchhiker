<?php

namespace App\Application\Controllers\Api;


use App\Application\Controllers\Controller;
use App\Application\Model\Contractstatus;
use App\Application\Transformers\ContractstatusTransformers;
use App\Application\Requests\Website\Contractstatus\ApiAddRequestContractstatus;
use App\Application\Requests\Website\Contractstatus\ApiUpdateRequestContractstatus;

class ContractstatusApi extends Controller
{
    use ApiTrait;
    protected $model;

    public function __construct(Contractstatus $model)
    {
        $this->model = $model;
        /// send header Authorization Bearer token
        /// $this->middleware('authApi')->only();
    }

    public function add(ApiAddRequestContractstatus $validation){
         return $this->addItem($validation);
    }

    public function update($id , ApiUpdateRequestContractstatus $validation){
        return $this->updateItem($id , $validation);
    }

    protected function checkLanguageBeforeReturn($data , $status_code = 200, $paginate = [])
    {
       if (request()->has('lang') && request()->get('lang') == 'ar') {
            return response(apiReturn(ContractstatusTransformers::transformAr($data) + $paginate), $status_code);
        }
        return response(apiReturn(ContractstatusTransformers::transform($data) + $paginate), $status_code);
    }

}
