<?php

namespace App\Application\Controllers\Api;


use App\Application\Controllers\Controller;
use App\Application\Model\Request;
use App\Application\Model\Contract;
use App\Application\Transformers\RequestTransformers;
use App\Application\Requests\Website\Request\ApiAddRequestRequest;
use App\Application\Requests\Website\Request\ApiUpdateRequestRequest;

class RequestApi extends Controller
{
    use ApiTrait;
    protected $model;

    public function __construct(Request $model)
    {
        $this->model = $model;
        /// send header Authorization Bearer token
         $this->middleware('authApi')->only(['pending','accept','reject']);
    }

    public function add(ApiAddRequestRequest $validation){
         return $this->addItem($validation);
    }

    public function update($id , ApiUpdateRequestRequest $validation){
        return $this->updateItem($id , $validation);
    }
    public function pending()
    {
        $user = auth()->guard('api')->user();
        $requests = $this->model
            ->where('hicker_id', $user->id)
            ->where('requeststatus_id', 1)
            ->orderBy('created_at', 'desc')->get();
        if ($requests) {
            return apiReturn(RequestTransformers::transform($requests));
        }
        return apiReturn([]);
    }
    public function accept()
    {
        $user = auth()->guard('api')->user();
        $request = $this->checkRequestType();
        $req = $this->model
            ->where('hicker_id', $user->id)
            ->where('requeststatus_id', 1)
            ->where('id', $request['id'])->with('user')->first();
        if($req){
            $req->requeststatus_id = 2;
            $req->save();
            $contract =  Contract::create([
                'user_id' => $req->user_id ,
                'hicker_id' => $req->hicker_id,
                'from_id' => $req->from_id,
                'to_id' => $req->to_id,
                'contractstatus_id' => $req->contractstatus_id ,
                'price' => $req->price
            ]);
            if($contract)
                return apiReturn('');
            return apiReturn('', 'false', 'something went wrong');     
        }
        return apiReturn('' , 'false' , 'no data found'); 

    }
    public function reject()
    {
        $user = auth()->guard('api')->user();
        $request = $this->checkRequestType();
        $req = $this->model
            ->where('hicker_id', $user->id)
            ->where('requeststatus_id', 1)
            ->where('id', $request['id'])->first();
        if ($req) {
            $req->requeststatus_id = 3;
            $req->save();
            return apiReturn('');
        }
        return apiReturn('', 'false', 'no data found');

    }
    protected function checkLanguageBeforeReturn($data , $status_code = 200, $paginate = [])
    {
       if (request()->has('lang') && request()->get('lang') == 'ar') {
            return response(apiReturn(RequestTransformers::transformAr($data) + $paginate), $status_code);
        }
        return response(apiReturn(RequestTransformers::transform($data) + $paginate), $status_code);
    }

}
