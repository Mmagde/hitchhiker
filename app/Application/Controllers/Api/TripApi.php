<?php

namespace App\Application\Controllers\Api;


use App\Application\Controllers\Controller;
use App\Application\Model\Trip;
use App\Application\Transformers\TripTransformers;
use App\Application\Requests\Website\Trip\ApiAddRequestTrip;
use App\Application\Requests\Website\Trip\ApiUpdateRequestTrip;

class TripApi extends Controller
{
    use ApiTrait;
    protected $model;

    public function __construct(Trip $model)
    {
        $this->model = $model;
        /// send header Authorization Bearer token
        /// $this->middleware('authApi')->only();
    }

    public function add(ApiAddRequestTrip $validation){
         return $this->addItem($validation);
    }
    public function search()
    {
        $request = $this->checkRequestType();
        $trips = $this->model
        ->where('from_id' , $request['from_id'])
        ->where('to_id', $request['to_id'])
        ->where('date' , '>=' , $request['date'])
        ->orderBy('created_at', 'desc')->get();
        if($trips){
          return  apiReturn(TripTransformers::transform($trips));
        }
        return apiReturn([]);
        
    }
    public function update($id , ApiUpdateRequestTrip $validation){
        return $this->updateItem($id , $validation);
    }

    protected function checkLanguageBeforeReturn($data , $status_code = 200, $paginate = [])
    {
       if (request()->has('lang') && request()->get('lang') == 'ar') {
            return response(apiReturn(TripTransformers::transformAr($data) + $paginate), $status_code);
        }
        return response(apiReturn(TripTransformers::transform($data) + $paginate), $status_code);
    }

}
