<?php

namespace App\Application\Controllers\Api;


use App\Application\Controllers\Controller;
use App\Application\Model\Type;
use App\Application\Transformers\TypeTransformers;
use App\Application\Requests\Website\Type\ApiAddRequestType;
use App\Application\Requests\Website\Type\ApiUpdateRequestType;

class TypeApi extends Controller
{
    use ApiTrait;
    protected $model;

    public function __construct(Type $model)
    {
        $this->model = $model;
        /// send header Authorization Bearer token
        /// $this->middleware('authApi')->only();
    }

    public function add(ApiAddRequestType $validation){
         return $this->addItem($validation);
    }

    public function update($id , ApiUpdateRequestType $validation){
        return $this->updateItem($id , $validation);
    }

    protected function checkLanguageBeforeReturn($data , $status_code = 200, $paginate = [])
    {
       if (request()->has('lang') && request()->get('lang') == 'ar') {
            return response(apiReturn(TypeTransformers::transformAr($data) + $paginate), $status_code);
        }
        return response(apiReturn(TypeTransformers::transform($data) + $paginate), $status_code);
    }

}
