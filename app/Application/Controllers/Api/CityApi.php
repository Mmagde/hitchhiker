<?php

namespace App\Application\Controllers\Api;


use App\Application\Controllers\Controller;
use App\Application\Model\City;
use App\Application\Transformers\CityTransformers;
use App\Application\Requests\Website\City\ApiAddRequestCity;
use App\Application\Requests\Website\City\ApiUpdateRequestCity;

class CityApi extends Controller
{
    use ApiTrait;
    protected $model;

    public function __construct(City $model)
    {
        $this->model = $model;
        /// send header Authorization Bearer token
        /// $this->middleware('authApi')->only();
    }

    public function add(ApiAddRequestCity $validation){
         return $this->addItem($validation);
    }

    public function update($id , ApiUpdateRequestCity $validation){
        return $this->updateItem($id , $validation);
    }

    protected function checkLanguageBeforeReturn($data , $status_code = 200, $paginate = [])
    {
       if (request()->has('lang') && request()->get('lang') == 'ar') {
            return response(apiReturn(CityTransformers::transformAr($data) + $paginate), $status_code);
        }
        return response(apiReturn(CityTransformers::transform($data) + $paginate), $status_code);
    }

}
