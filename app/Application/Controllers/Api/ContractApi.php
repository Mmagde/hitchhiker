<?php

namespace App\Application\Controllers\Api;


use App\Application\Controllers\Controller;
use App\Application\Model\Contract;
use App\Application\Transformers\ContractTransformers;
use App\Application\Requests\Website\Contract\ApiAddRequestContract;
use App\Application\Requests\Website\Contract\ApiUpdateRequestContract;

class ContractApi extends Controller
{
    use ApiTrait;
    protected $model;

    public function __construct(Contract $model)
    {
        $this->model = $model;
        /// send header Authorization Bearer token
        $this->middleware('authApi');
    }

    public function add(ApiAddRequestContract $validation){
         return $this->addItem($validation);
    }
    public function userContracts()
    {
        $user = auth()->guard('api')->user();
        $contracts = $this->model->where('user_id' , $user->id)->with('contractstatus')->get();
        if($contracts)
            return apiReturn(ContractTransformers::transform($contracts));
        return apiReturn([]);    

    }
    public function hickerContracts()
    {
        $user = auth()->guard('api')->user();
        $contracts = $this->model->where('hicker_id', $user->id)->with('contractstatus')->get();
        if($contracts)
            return apiReturn(ContractTransformers::transform($contracts));
        return apiReturn([]);
        
    }
    public function doneContracts()
    {
        $user = auth()->guard('api')->user();
        $request = $this->checkRequestType();
        $contract = $this->model->where('id', $request['id'])->first();
        if ($contract){
            $contract->contractstatus_id = 2;
            $contract->save();
            return apiReturn('');
        }
        return apiReturn('', 'false' , 'No Data Found');
    }
    public function cancelContracts()
    {
        $user = auth()->guard('api')->user();
        $request = $this->checkRequestType();
        $contract = $this->model->where('id', $request['id'])->first();
        if ($contract) {
            $contract->contractstatus_id = 3;
            $contract->save();
            return apiReturn('');
        }
        return apiReturn('', 'false', 'No Data Found');
    }
    public function update($id , ApiUpdateRequestContract $validation){
        return $this->updateItem($id , $validation);
    }

    protected function checkLanguageBeforeReturn($data , $status_code = 200, $paginate = [])
    {
       if (request()->has('lang') && request()->get('lang') == 'ar') {
            return response(apiReturn(ContractTransformers::transformAr($data) + $paginate), $status_code);
        }
        return response(apiReturn(ContractTransformers::transform($data) + $paginate), $status_code);
    }

}
