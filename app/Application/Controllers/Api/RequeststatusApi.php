<?php

namespace App\Application\Controllers\Api;


use App\Application\Controllers\Controller;
use App\Application\Model\Requeststatus;
use App\Application\Transformers\RequeststatusTransformers;
use App\Application\Requests\Website\Requeststatus\ApiAddRequestRequeststatus;
use App\Application\Requests\Website\Requeststatus\ApiUpdateRequestRequeststatus;

class RequeststatusApi extends Controller
{
    use ApiTrait;
    protected $model;

    public function __construct(Requeststatus $model)
    {
        $this->model = $model;
        /// send header Authorization Bearer token
        /// $this->middleware('authApi')->only();
    }

    public function add(ApiAddRequestRequeststatus $validation){
         return $this->addItem($validation);
    }

    public function update($id , ApiUpdateRequestRequeststatus $validation){
        return $this->updateItem($id , $validation);
    }

    protected function checkLanguageBeforeReturn($data , $status_code = 200, $paginate = [])
    {
       if (request()->has('lang') && request()->get('lang') == 'ar') {
            return response(apiReturn(RequeststatusTransformers::transformAr($data) + $paginate), $status_code);
        }
        return response(apiReturn(RequeststatusTransformers::transform($data) + $paginate), $status_code);
    }

}
