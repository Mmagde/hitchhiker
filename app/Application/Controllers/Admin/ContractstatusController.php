<?php

namespace App\Application\Controllers\Admin;

use App\Application\Requests\Admin\Contractstatus\AddRequestContractstatus;
use App\Application\Requests\Admin\Contractstatus\UpdateRequestContractstatus;
use App\Application\Controllers\AbstractController;
use App\Application\DataTables\ContractstatussDataTable;
use App\Application\Model\Contractstatus;
use Yajra\Datatables\Request;
use Alert;

class ContractstatusController extends AbstractController
{
    public function __construct(Contractstatus $model)
    {
        parent::__construct($model);
    }

    public function index(ContractstatussDataTable $dataTable){
        return $dataTable->render('admin.contractstatus.index');
    }

    public function show($id = null){
        return $this->createOrEdit('admin.contractstatus.edit' , $id);
    }

     public function store(AddRequestContractstatus $request){
          $item =  $this->storeOrUpdate($request , null , true);
          return redirect('admin/contractstatus');
     }

     public function update($id , UpdateRequestContractstatus $request){
          $item = $this->storeOrUpdate($request, $id, true);
return redirect()->back();

     }


    public function getById($id){
        $fields = $this->model->findOrFail($id);
        return $this->createOrEdit('admin.contractstatus.show' , $id , ['fields' =>  $fields]);
    }

    public function destroy($id){
        return $this->deleteItem($id , 'admin/contractstatus')->with('sucess' , 'Done Delete contractstatus From system');
    }

    public function pluck(\Illuminate\Http\Request $request){
        return $this->deleteItem($request->id , 'admin/contractstatus')->with('sucess' , 'Done Delete contractstatus From system');
    }

}
