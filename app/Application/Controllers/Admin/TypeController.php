<?php

namespace App\Application\Controllers\Admin;

use App\Application\Requests\Admin\Type\AddRequestType;
use App\Application\Requests\Admin\Type\UpdateRequestType;
use App\Application\Controllers\AbstractController;
use App\Application\DataTables\TypesDataTable;
use App\Application\Model\Type;
use Yajra\Datatables\Request;
use Alert;

class TypeController extends AbstractController
{
    public function __construct(Type $model)
    {
        parent::__construct($model);
    }

    public function index(TypesDataTable $dataTable){
        return $dataTable->render('admin.type.index');
    }

    public function show($id = null){
        return $this->createOrEdit('admin.type.edit' , $id);
    }

     public function store(AddRequestType $request){
          $item =  $this->storeOrUpdate($request , null , true);
          return redirect('admin/type');
     }

     public function update($id , UpdateRequestType $request){
          $item = $this->storeOrUpdate($request, $id, true);
return redirect()->back();

     }


    public function getById($id){
        $fields = $this->model->findOrFail($id);
        return $this->createOrEdit('admin.type.show' , $id , ['fields' =>  $fields]);
    }

    public function destroy($id){
        return $this->deleteItem($id , 'admin/type')->with('sucess' , 'Done Delete type From system');
    }

    public function pluck(\Illuminate\Http\Request $request){
        return $this->deleteItem($request->id , 'admin/type')->with('sucess' , 'Done Delete type From system');
    }

}
