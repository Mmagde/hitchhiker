<?php

namespace App\Application\Controllers\Admin;

use App\Application\Requests\Admin\City\AddRequestCity;
use App\Application\Requests\Admin\City\UpdateRequestCity;
use App\Application\Controllers\AbstractController;
use App\Application\DataTables\CitysDataTable;
use App\Application\Model\City;
use Yajra\Datatables\Request;
use Alert;

class CityController extends AbstractController
{
    public function __construct(City $model)
    {
        parent::__construct($model);
    }

    public function index(CitysDataTable $dataTable){
        return $dataTable->render('admin.city.index');
    }

    public function show($id = null){
        return $this->createOrEdit('admin.city.edit' , $id);
    }

     public function store(AddRequestCity $request){
          $item =  $this->storeOrUpdate($request , null , true);
          return redirect('admin/city');
     }

     public function update($id , UpdateRequestCity $request){
          $item = $this->storeOrUpdate($request, $id, true);
return redirect()->back();

     }


    public function getById($id){
        $fields = $this->model->findOrFail($id);
        return $this->createOrEdit('admin.city.show' , $id , ['fields' =>  $fields]);
    }

    public function destroy($id){
        return $this->deleteItem($id , 'admin/city')->with('sucess' , 'Done Delete city From system');
    }

    public function pluck(\Illuminate\Http\Request $request){
        return $this->deleteItem($request->id , 'admin/city')->with('sucess' , 'Done Delete city From system');
    }

}
