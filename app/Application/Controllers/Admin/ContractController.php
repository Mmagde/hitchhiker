<?php

namespace App\Application\Controllers\Admin;

use App\Application\Requests\Admin\Contract\AddRequestContract;
use App\Application\Requests\Admin\Contract\UpdateRequestContract;
use App\Application\Controllers\AbstractController;
use App\Application\DataTables\ContractsDataTable;
use App\Application\Model\Contract;
use Yajra\Datatables\Request;
use Alert;

class ContractController extends AbstractController
{
    public function __construct(Contract $model)
    {
        parent::__construct($model);
    }

    public function index(ContractsDataTable $dataTable){
        return $dataTable->render('admin.contract.index');
    }

    public function show($id = null){
        return $this->createOrEdit('admin.contract.edit' , $id);
    }

     public function store(AddRequestContract $request){
          $item =  $this->storeOrUpdate($request , null , true);
          return redirect('admin/contract');
     }

     public function update($id , UpdateRequestContract $request){
          $item = $this->storeOrUpdate($request, $id, true);
return redirect()->back();

     }


    public function getById($id){
        $fields = $this->model->findOrFail($id);
        return $this->createOrEdit('admin.contract.show' , $id , ['fields' =>  $fields]);
    }

    public function destroy($id){
        return $this->deleteItem($id , 'admin/contract')->with('sucess' , 'Done Delete contract From system');
    }

    public function pluck(\Illuminate\Http\Request $request){
        return $this->deleteItem($request->id , 'admin/contract')->with('sucess' , 'Done Delete contract From system');
    }

}
