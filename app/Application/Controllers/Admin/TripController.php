<?php

namespace App\Application\Controllers\Admin;

use App\Application\Requests\Admin\Trip\AddRequestTrip;
use App\Application\Requests\Admin\Trip\UpdateRequestTrip;
use App\Application\Controllers\AbstractController;
use App\Application\DataTables\TripsDataTable;
use App\Application\Model\Trip;
use Yajra\Datatables\Request;
use Alert;

class TripController extends AbstractController
{
    public function __construct(Trip $model)
    {
        parent::__construct($model);
    }

    public function index(TripsDataTable $dataTable){
        return $dataTable->render('admin.trip.index');
    }

    public function show($id = null){
        return $this->createOrEdit('admin.trip.edit' , $id);
    }

     public function store(AddRequestTrip $request){
          $item =  $this->storeOrUpdate($request , null , true);
          return redirect('admin/trip');
     }

     public function update($id , UpdateRequestTrip $request){
          $item = $this->storeOrUpdate($request, $id, true);
return redirect()->back();

     }


    public function getById($id){
        $fields = $this->model->findOrFail($id);
        return $this->createOrEdit('admin.trip.show' , $id , ['fields' =>  $fields]);
    }

    public function destroy($id){
        return $this->deleteItem($id , 'admin/trip')->with('sucess' , 'Done Delete trip From system');
    }

    public function pluck(\Illuminate\Http\Request $request){
        return $this->deleteItem($request->id , 'admin/trip')->with('sucess' , 'Done Delete trip From system');
    }

}
