<?php

namespace App\Application\Controllers\Admin;

use App\Application\Requests\Admin\Requeststatus\AddRequestRequeststatus;
use App\Application\Requests\Admin\Requeststatus\UpdateRequestRequeststatus;
use App\Application\Controllers\AbstractController;
use App\Application\DataTables\RequeststatussDataTable;
use App\Application\Model\Requeststatus;
use Yajra\Datatables\Request;
use Alert;

class RequeststatusController extends AbstractController
{
    public function __construct(Requeststatus $model)
    {
        parent::__construct($model);
    }

    public function index(RequeststatussDataTable $dataTable){
        return $dataTable->render('admin.requeststatus.index');
    }

    public function show($id = null){
        return $this->createOrEdit('admin.requeststatus.edit' , $id);
    }

     public function store(AddRequestRequeststatus $request){
          $item =  $this->storeOrUpdate($request , null , true);
          return redirect('admin/requeststatus');
     }

     public function update($id , UpdateRequestRequeststatus $request){
          $item = $this->storeOrUpdate($request, $id, true);
return redirect()->back();

     }


    public function getById($id){
        $fields = $this->model->findOrFail($id);
        return $this->createOrEdit('admin.requeststatus.show' , $id , ['fields' =>  $fields]);
    }

    public function destroy($id){
        return $this->deleteItem($id , 'admin/requeststatus')->with('sucess' , 'Done Delete requeststatus From system');
    }

    public function pluck(\Illuminate\Http\Request $request){
        return $this->deleteItem($request->id , 'admin/requeststatus')->with('sucess' , 'Done Delete requeststatus From system');
    }

}
