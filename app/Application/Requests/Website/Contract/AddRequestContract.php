<?php
 namespace App\Application\Requests\Website\Contract;
 use Illuminate\Foundation\Http\FormRequest;
 class AddRequestContract extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
     /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
        	"user_id" => "required|integer",
         "city_id" => "required|integer",
         "contractstatus_id" => "required|integer",
            "price" => "",
   "status" => "",
            ];
    }
}
