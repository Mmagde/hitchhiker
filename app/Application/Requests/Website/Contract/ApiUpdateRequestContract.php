<?php
 namespace App\Application\Requests\Website\Contract;
 use Illuminate\Support\Facades\Route;
 class ApiUpdateRequestContract
{
    public function rules()
    {
        $id = Route::input('id');
        return [
            "user_id" => "required|integer",
            "hicker_id" => "required|integer",
            "from_id" => "required|integer",
            "to_id" => "required|integer",
         "contractstatus_id" => "required|integer",
            "price" => "",
   "status" => "",
            ];
    }
}
