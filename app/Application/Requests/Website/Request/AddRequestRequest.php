<?php
 namespace App\Application\Requests\Website\Request;
 use Illuminate\Foundation\Http\FormRequest;
 class AddRequestRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
     /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
        	"city_id" => "required|integer",
         "user_id" => "required|integer",
         "requeststatus_id" => "required|integer",
            "price" => "",
   "status" => "",
            ];
    }
}
