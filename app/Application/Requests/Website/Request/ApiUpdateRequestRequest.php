<?php
 namespace App\Application\Requests\Website\Request;
 use Illuminate\Support\Facades\Route;
 class ApiUpdateRequestRequest
{
    public function rules()
    {
        $id = Route::input('id');
        return [
            "from_id" => "required|integer",
            "to_id" => "required|integer",
         "user_id" => "required|integer",
         "requeststatus_id" => "required|integer",
            "price" => "",
   "status" => "",
            ];
    }
}
