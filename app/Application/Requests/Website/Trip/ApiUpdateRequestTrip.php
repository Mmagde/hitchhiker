<?php
 namespace App\Application\Requests\Website\Trip;
 use Illuminate\Support\Facades\Route;
 class ApiUpdateRequestTrip
{
    public function rules()
    {
        $id = Route::input('id');
        return [
            "from_id" => "required|integer",
            "to_id" => "required|integer",
         "user_id" => "required|integer",
            "phone" => "date",
   "price" => "",
            ];
    }
}
