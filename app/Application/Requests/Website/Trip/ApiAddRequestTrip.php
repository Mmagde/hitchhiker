<?php
 namespace App\Application\Requests\Website\Trip;
  class ApiAddRequestTrip
{
    public function rules()
    {
        return [
            "from_id" => "required|integer",
            "to_id" => "required|integer",
            "user_id" => "required|integer",
            "phone" => "",
            "date" => "",
            "price" => "",
            ];
    }
}
