<?php
 namespace App\Application\Requests\Admin\Trip;
 use Illuminate\Foundation\Http\FormRequest;
 class AddRequestTrip extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
     /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "from_id" => "required|integer",
            "to_id" => "required|integer",
         "user_id" => "required|integer",
            "phone" => "",
   "date" => "",
   "price" => "",
            ];
    }
}
