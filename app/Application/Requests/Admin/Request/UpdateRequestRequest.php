<?php
 namespace App\Application\Requests\Admin\Request;
 use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Route;
 class UpdateRequestRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
     /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id = Route::input('id');
        return [
            "from_id" => "required|integer",
            "hicker_id" => "required|integer",
            "to_id" => "required|integer",
         "user_id" => "required|integer",
         "requeststatus_id" => "required|integer",
            "price" => "",
   "status" => "",
            ];
    }
}
