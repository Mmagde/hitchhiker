<?php

namespace App\Application\Transformers;

use Illuminate\Database\Eloquent\Model;

class ContractTransformers extends AbstractTransformer
{

    public function transformModel(Model $modelOrCollection)
    {
        return [
            "id" => $modelOrCollection->id,
			"price" => $modelOrCollection->price,
            "contractstatus" => $modelOrCollection->contractstatus->name ,
            "from_id" => $modelOrCollection->from_id,
            "to_id" => $modelOrCollection->to_id,
        ];
    }

    public function transformModelAr(Model $modelOrCollection)
    {
        return [
           "id" => $modelOrCollection->id,
            "price" => $modelOrCollection->price,
            "contractstatus" => $modelOrCollection->contractstatus->name,
            "from_id" => $modelOrCollection->from_id,
            "to_id" => $modelOrCollection->to_id,
        ];
    }

}