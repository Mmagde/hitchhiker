<?php

namespace App\Application\Transformers;

use Illuminate\Database\Eloquent\Model;

class RequestTransformers extends AbstractTransformer
{

    public function transformModel(Model $modelOrCollection)
    {
        return [
            "id" => $modelOrCollection->id,
			"price" => $modelOrCollection->price,
            "from_id" => $modelOrCollection->from_id ,
            "to_id" => $modelOrCollection->to_id ,
            "name" => $modelOrCollection->user->name

        ];
    }

    public function transformModelAr(Model $modelOrCollection)
    {
        return [
           "id" => $modelOrCollection->id,
			"price" => $modelOrCollection->price,
            "from_id" => $modelOrCollection->from_id,
            "to_id" => $modelOrCollection->to_id,
            "name" => $modelOrCollection->user->name
        ];
    }

}