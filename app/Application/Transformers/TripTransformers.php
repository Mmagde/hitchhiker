<?php

namespace App\Application\Transformers;

use Illuminate\Database\Eloquent\Model;

class TripTransformers extends AbstractTransformer
{

    public function transformModel(Model $modelOrCollection)
    {
        return [
            "id" => $modelOrCollection->id,
			"phone" => $modelOrCollection->phone,
			"date" => $modelOrCollection->date,
            "price" => $modelOrCollection->price,
            "from_id" => $modelOrCollection->from_id,
            "to_id" => $modelOrCollection->to_id,
            "user_id" => $modelOrCollection->user_id,

        ];
    }

    public function transformModelAr(Model $modelOrCollection)
    {
        return [
           "id" => $modelOrCollection->id,
			"phone" => $modelOrCollection->phone,
			"date" => $modelOrCollection->date,
			"price" => $modelOrCollection->price,
            "from_id" => $modelOrCollection->from_id,
            "to_id" => $modelOrCollection->to_id,
            "user_id" => $modelOrCollection->user_id,
        ];
    }

}